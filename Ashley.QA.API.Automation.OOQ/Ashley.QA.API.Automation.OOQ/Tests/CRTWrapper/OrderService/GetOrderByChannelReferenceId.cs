﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashley.QA.APILibrary;

namespace Ashley.QA.API.Automation.OOQ.Tests.CRTWrapper.OrderService
{
    /// <summary>
    /// Summary description for GetOrderByChannelReferenceId
    /// </summary>
    [TestClass]
    public class GetOrderByChannelReferenceId
    {
        public GetOrderByChannelReferenceId()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        

        [TestMethod]
        [TestCategory("GetOrderByChannelReferenceId")]
        [WorkItem(135971)]
        [Description("Verify the GetOrderByChannelReferenceId method with valid input parameters")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "135971", DataAccessMethod.Sequential)]
        public void VerifyTheGetorderbychannelreferenceidMethodWithValidInputParameters()
        {
            #region Local Variables

            Object response = null;

            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string headerName_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[0];
            string headerValue_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[1];

            string headerName_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[0];
            string headerValue_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[1];

            string headerName_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[0];
            string headerValue_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[1];
            
            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();


            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_confirmationId, headerValue_confirmationId);
            get.AddQueryString(headerName_billingEmail, headerValue_billingEmail);
            get.AddQueryString(headerName_billingZipCode, headerValue_billingZipCode);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            
            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetOrderByChannelReferenceId")]
        [WorkItem(136027)]
        [Description("Verify the GetOrderByChannelReferenceId with an invalid confirmationId")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "136027", DataAccessMethod.Sequential)]
        public void VerifyTheGetOrderByChannelReferenceIdWithAnInvalidConfirmationId()
        {
            #region Local Variables

            Object response = null;
            Object actualSalesOrder = null;
            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string headerName_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[0];
            string headerValue_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[1];

            string headerName_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[0];
            string headerValue_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[1];

            string headerName_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[0];
            string headerValue_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[1];

            string salesOrder = TestContext.DataRow["SalesOrder"].ToString();

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();


            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_confirmationId, headerValue_confirmationId);
            get.AddQueryString(headerName_billingEmail, headerValue_billingEmail);
            get.AddQueryString(headerName_billingZipCode, headerValue_billingZipCode);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualSalesOrder = string.IsNullOrEmpty((string)response.ValidateResponse("SalesOrder")) == true ? "None" : response.ValidateResponse("SalesOrder");
            Assert.AreEqual(salesOrder, actualSalesOrder, "SalesOrder is displayed for the given invalid request : " + actualSalesOrder);

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetOrderByChannelReferenceId")]
        [WorkItem(136037)]
        [Description("Verify the GetOrderByChannelReferenceId with an invalid billingEmail")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "136037", DataAccessMethod.Sequential)]
        public void VerifyTheGetOrderByChannelReferenceIdWithAnInvalidBillingEmail()
        {
            #region Local Variables

            Object response = null;
            Object actualSalesOrder = null;
            Object actualErrorCode = null;
            Object actualErrorMessage = null;
            Object actualShowDebugInfo = null;
            Object actualExtendedErrorMessage = null;
            Object actualSource = null;
            Object actualStackTrace = null;

            Object actualCanRetry = null;
            Object actualRedirectUrl = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string headerName_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[0];
            string headerValue_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[1];

            string headerName_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[0];
            string headerValue_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[1];

            string headerName_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[0];
            string headerValue_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[1];

            string salesOrder = TestContext.DataRow["SalesOrder"].ToString();
            string errorCode = TestContext.DataRow["ErrorCode"].ToString();
            string errorMessage = TestContext.DataRow["ErrorMessage"].ToString();
            string showDebugInfo = TestContext.DataRow["ShowDebugInfo"].ToString();
            string extendedErrorMessage = TestContext.DataRow["ExtendedErrorMessage"].ToString();
            string source = TestContext.DataRow["Source"].ToString();
            string stackTrace = TestContext.DataRow["StackTrace"].ToString();

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_confirmationId, headerValue_confirmationId);
            get.AddQueryString(headerName_billingEmail, headerValue_billingEmail);
            get.AddQueryString(headerName_billingZipCode, headerValue_billingZipCode);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualErrorCode = response.ValidateResponse("ErrorCode", "Errors-0");
            Assert.AreEqual(errorCode, actualErrorCode, "Expected ErrorCode : " + errorCode + " does not match with Actual value : " + actualErrorCode);

            actualErrorMessage = response.ValidateResponse("ErrorMessage", "Errors-0");
            Assert.AreEqual(errorMessage, actualErrorMessage, "Expected ErrorMessage : " + errorMessage + " does not match with Actual value : " + actualErrorMessage);

            actualShowDebugInfo = response.ValidateResponse("ShowDebugInfo", "Errors-0");
            Assert.AreEqual(showDebugInfo, actualShowDebugInfo, "Expected ShowDebugInfo  : " + showDebugInfo + " does not match with Actual value : " + actualShowDebugInfo);

            actualExtendedErrorMessage = string.IsNullOrEmpty((string)response.ValidateResponse("ExtendedErrorMessage", "Errors-0")) == true ? "None" : response.ValidateResponse("ExtendedErrorMessage", "Errors-0");
            Assert.AreEqual(extendedErrorMessage, actualExtendedErrorMessage, "ExtendedErrorMessage is displayed for the given invalid request : " + actualExtendedErrorMessage);

            actualSource = string.IsNullOrEmpty((string)response.ValidateResponse("Source", "Errors-0")) == true ? "None" : response.ValidateResponse("Source", "Errors-0");
            Assert.AreEqual(source, actualSource, "Source is displayed for the given invalid request : " + actualSource);

            actualStackTrace = string.IsNullOrEmpty((string)response.ValidateResponse("StackTrace", "Errors-0")) == true ? "None" : response.ValidateResponse("StackTrace", "Errors-0");
            Assert.AreEqual(stackTrace, actualStackTrace, "StackTrace is displayed for the given invalid request : " + actualStackTrace);

            actualSalesOrder = string.IsNullOrEmpty((string)response.ValidateResponse("SalesOrder")) == true ? "None" : response.ValidateResponse("SalesOrder");
            Assert.AreEqual(salesOrder, actualSalesOrder, "SalesOrder is displayed for the given invalid request : " + actualSalesOrder);

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);
            
            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetOrderByChannelReferenceId")]
        [WorkItem(136039)]
        [Description("Verify the GetOrderByChannelReferenceId with an invalid billingZipCode")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "136039", DataAccessMethod.Sequential)]
        public void VerifyTheGetOrderByChannelReferenceIdWithAnInvalidBillingZipCode()
        {
            #region Local Variables

            Object response = null;
            Object actualSalesOrder = null;
            Object actualErrorCode = null;
            Object actualErrorMessage = null;
            Object actualShowDebugInfo = null;
            Object actualExtendedErrorMessage = null;
            Object actualSource = null;
            Object actualStackTrace = null;

            Object actualCanRetry = null;
            Object actualRedirectUrl = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string headerName_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[0];
            string headerValue_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[1];

            string headerName_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[0];
            string headerValue_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[1];

            string headerName_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[0];
            string headerValue_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[1];

            string salesOrder = TestContext.DataRow["SalesOrder"].ToString();
            string errorCode = TestContext.DataRow["ErrorCode"].ToString();
            string errorMessage = TestContext.DataRow["ErrorMessage"].ToString();
            string showDebugInfo = TestContext.DataRow["ShowDebugInfo"].ToString();
            string extendedErrorMessage = TestContext.DataRow["ExtendedErrorMessage"].ToString();
            string source = TestContext.DataRow["Source"].ToString();
            string stackTrace = TestContext.DataRow["StackTrace"].ToString();

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();


            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_confirmationId, headerValue_confirmationId);
            get.AddQueryString(headerName_billingEmail, headerValue_billingEmail);
            get.AddQueryString(headerName_billingZipCode, headerValue_billingZipCode);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualErrorCode = response.ValidateResponse("ErrorCode", "Errors-0");
            Assert.AreEqual(errorCode, actualErrorCode, "Expected ErrorCode : " + errorCode + " does not match with Actual value : " + actualErrorCode);

            actualErrorMessage = response.ValidateResponse("ErrorMessage", "Errors-0");
            Assert.AreEqual(errorMessage, actualErrorMessage, "Expected ErrorMessage : " + errorMessage + " does not match with Actual value : " + actualErrorMessage);

            actualShowDebugInfo = response.ValidateResponse("ShowDebugInfo", "Errors-0");
            Assert.AreEqual(showDebugInfo, actualShowDebugInfo, "Expected ShowDebugInfo  : " + showDebugInfo + " does not match with Actual value : " + actualShowDebugInfo);

            actualExtendedErrorMessage = string.IsNullOrEmpty((string)response.ValidateResponse("ExtendedErrorMessage", "Errors-0")) == true ? "None" : response.ValidateResponse("ExtendedErrorMessage", "Errors-0");
            Assert.AreEqual(extendedErrorMessage, actualExtendedErrorMessage, "ExtendedErrorMessage is displayed for the given invalid request : " + actualExtendedErrorMessage);

            actualSource = string.IsNullOrEmpty((string)response.ValidateResponse("Source", "Errors-0")) == true ? "None" : response.ValidateResponse("Source", "Errors-0");
            Assert.AreEqual(source, actualSource, "Source is displayed for the given invalid request : " + actualSource);

            actualStackTrace = string.IsNullOrEmpty((string)response.ValidateResponse("StackTrace", "Errors-0")) == true ? "None" : response.ValidateResponse("StackTrace", "Errors-0");
            Assert.AreEqual(stackTrace, actualStackTrace, "StackTrace is displayed for the given invalid request : " + actualStackTrace);

            actualSalesOrder = string.IsNullOrEmpty((string)response.ValidateResponse("SalesOrder")) == true ? "None" : response.ValidateResponse("SalesOrder");
            Assert.AreEqual(salesOrder, actualSalesOrder, "SalesOrder is displayed for the given invalid request : " + actualSalesOrder);

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);
            
            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetOrderByChannelReferenceId")]
        [WorkItem(136040)]
        [Description("Verify the GetOrderByChannelReferenceId with an invalid resourcePath")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "136040", DataAccessMethod.Sequential)]
        public void VerifyTheGetOrderByChannelReferenceIdWithAnInvalidResourcePath()
        {
            #region Local Variables

            Object response = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string headerName_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[0];
            string headerValue_confirmationId = TestContext.DataRow["confirmationId"].ToString().Split('=')[1];

            string headerName_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[0];
            string headerValue_billingEmail = TestContext.DataRow["billingEmail"].ToString().Split('=')[1];

            string headerName_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[0];
            string headerValue_billingZipCode = TestContext.DataRow["billingZipCode"].ToString().Split('=')[1];

            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[0].Trim();
            string statusDescription = TestContext.DataRow["StatusDescription"].ToString();

                        #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_confirmationId, headerValue_confirmationId);
            get.AddQueryString(headerName_billingEmail, headerValue_billingEmail);
            get.AddQueryString(headerName_billingZipCode, headerValue_billingZipCode);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response as - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "State Provinces are coming for given invalid request : " + get.ResponseContent);

            #endregion

            #endregion

        }
        


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        
    }
}
