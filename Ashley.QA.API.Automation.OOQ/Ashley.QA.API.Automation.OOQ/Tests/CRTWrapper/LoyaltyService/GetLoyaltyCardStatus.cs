﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashley.QA.APILibrary;

namespace Ashley.QA.API.Automation.OOQ.Tests.CRTWrapper.LoyaltyService
{
    /// <summary>
    /// Summary description for GetLoyaltyCardStatus
    /// </summary>
    [TestClass]
    public class GetLoyaltyCardStatus
    {
        public GetLoyaltyCardStatus()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        [TestMethod]
        [TestCategory("GetLoyaltyCardStatus")]
        [WorkItem(134434)]
        [Description("Verify the GetLoyaltyCardStatus method with a valid loyalty card number")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134434", DataAccessMethod.Sequential)]
        public void VerifyTheGetloyaltycardstatusMethodWithAValidLoyaltyCardNumber()
        {
            #region Local Variables

            Object response = null;
            Object actualCardNumber = null;
            Object actualCardTenderType = null;
            Object actualCustomerAccount = null;
            Object actualLoyaltyGroupsRecordId1 = null;
            Object actualLoyaltyGroupsName = null;
            Object actualLoyaltyGroupsDescription = null;
            Object actualLoyaltyTiersRecordId1 = null;
            Object actualLoyaltyTiersDescription1 = null;
            Object actualLoyaltyTiersTierId1 = null;
            Object actualLoyaltyTiersTierLevel1 = null;
            Object actualLoyaltyTiersRecordId2 = null;
            Object actualLoyaltyTiersDescription2 = null;
            Object actualLoyaltyTiersTierId2 = null;
            Object actualLoyaltyTiersTierLevel2 = null;
            Object actualRewardPointsDescription = null;
            Object actualRewardPointsIsRedeemable = null;
            Object actualRewardPointsRewardPointCurrency = null;
            Object actualRewardPointsRewardPointId = null;
            Object actualRewardPointsIssuedPoints = null;
            Object actualRewardPointsUsedPoints = null;
            Object actualRewardPointsExpiredPoints = null;
            Object actualRewardPointsActivePoints = null;

            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string headerName = TestContext.DataRow["loyaltyCardNumber"].ToString().Split('=')[0];
            string headerValue = TestContext.DataRow["loyaltyCardNumber"].ToString().Split('=')[1];

            string cardNUmber = TestContext.DataRow["CardNumber"].ToString();
            string cardTenderType = TestContext.DataRow["CardTenderType"].ToString();
            string customerAccount = TestContext.DataRow["CustomerAccount"].ToString();
            string loyaltyGroupsRecordId1 = TestContext.DataRow["LoyaltyGroupsRecordId"].ToString();
            string loyaltyGroupsName = TestContext.DataRow["LoyaltyGroupsName"].ToString();
            string loyaltyGroupsDescription = TestContext.DataRow["LoyaltyGroupsDescription"].ToString();
            string loyaltyTiersRecordId1 = TestContext.DataRow["LoyaltyTiersRecordId1"].ToString();
            string loyaltyTiersDescription1 = TestContext.DataRow["LoyaltyTiersDescription1"].ToString();
            string loyaltyTiersTierId1 = TestContext.DataRow["LoyaltyTiersTierId1"].ToString();
            string loyaltyTiersTierLevel1 = TestContext.DataRow["LoyaltyTiersTierLevel1"].ToString();
            string loyaltyTiersRecordId2 = TestContext.DataRow["LoyaltyTiersRecordId2"].ToString();
            string loyaltyTiersDescription2 = TestContext.DataRow["LoyaltyTiersDescription2"].ToString();
            string loyaltyTiersTierId2 = TestContext.DataRow["LoyaltyTiersTierId2"].ToString();
            string loyaltyTiersTierLevel2 = TestContext.DataRow["LoyaltyTiersTierLevel2"].ToString();
            string rewardPointsDescription = TestContext.DataRow["RewardPointsDescription"].ToString();
            string rewardPointsIsRedeemable = TestContext.DataRow["RewardPointsIsRedeemable"].ToString();
            string rewardPointsRewardPointCurrency = TestContext.DataRow["RewardPointsRewardPointCurrency"].ToString();
            string rewardPointsRewardPointId = TestContext.DataRow["RewardPointsRewardPointId"].ToString();
            string rewardPointsIssuedPoints = TestContext.DataRow["RewardPointsIssuedPoints"].ToString();
            string rewardPointsUsedPoints = TestContext.DataRow["RewardPointsUsedPoints"].ToString();
            string rewardPointsExpiredPoints = TestContext.DataRow["RewardPointsExpiredPoints"].ToString();
            string rewardPointsActivePoints = TestContext.DataRow["RewardPointsActivePoints"].ToString();

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();


            #endregion

            #region TestSteps
            
            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName, headerValue);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualCardNumber = response.ValidateResponse("CardNumber", "LoyaltyCard");
            Assert.AreEqual(cardNUmber, actualCardNumber, "Expected CardNumber : " + cardNUmber + " does not match with Actual value : " + actualCardNumber);

            actualCardTenderType = response.ValidateResponse("CardTenderType", "LoyaltyCard");
            Assert.AreEqual(cardTenderType, actualCardTenderType, "Expected CardTenderType : " + cardTenderType + " does not match with Actual value : " + actualCardTenderType);

            actualCustomerAccount = response.ValidateResponse("CustomerAccount", "LoyaltyCard");
            Assert.AreEqual(customerAccount, actualCustomerAccount, "Expected CustomerAccount : " + customerAccount + " does not match with Actual value : " + actualCustomerAccount);

            actualLoyaltyGroupsRecordId1 = response.ValidateResponse("RecordId", "LoyaltyCard,LoyaltyGroups-0");
            Assert.AreEqual(loyaltyGroupsRecordId1, actualLoyaltyGroupsRecordId1, "Expected CompanyCurrency : " + loyaltyGroupsRecordId1 + " does not match with Actual value : " + actualLoyaltyGroupsRecordId1);

            actualLoyaltyGroupsName = response.ValidateResponse("Name", "LoyaltyCard,LoyaltyGroups-0");
            Assert.AreEqual(loyaltyGroupsName, actualLoyaltyGroupsName, "Expected LoyaltyGroupsName : " + loyaltyGroupsName + " does not match with Actual value : " + actualLoyaltyGroupsName);

            actualLoyaltyGroupsDescription = response.ValidateResponse("Description", "LoyaltyCard,LoyaltyGroups-0");
            Assert.AreEqual(loyaltyGroupsDescription, actualLoyaltyGroupsDescription, "Expected LoyaltyGroupsDescription : " + loyaltyGroupsDescription + " does not match with Actual value : " + actualLoyaltyGroupsDescription);

            actualLoyaltyTiersRecordId1 = response.ValidateResponse("RecordId", "LoyaltyCard,LoyaltyGroups-0,LoyaltyTiers-0");
            Assert.AreEqual(loyaltyTiersRecordId1, actualLoyaltyTiersRecordId1, "Expected LoyaltyTiersRecordId : " + loyaltyTiersRecordId1 + " does not match with Actual value : " + actualLoyaltyTiersRecordId1);

            actualLoyaltyTiersDescription1 = response.ValidateResponse("Description", "LoyaltyCard,LoyaltyGroups-0,LoyaltyTiers-0");
            Assert.AreEqual(loyaltyTiersDescription1, actualLoyaltyTiersDescription1, "Expected LoyaltyTiersDescription : " + loyaltyTiersDescription1 + " does not match with Actual value : " + actualLoyaltyTiersDescription1);

            actualLoyaltyTiersTierId1 = response.ValidateResponse("TierId", "LoyaltyCard,LoyaltyGroups-0,LoyaltyTiers-0");
            Assert.AreEqual(loyaltyTiersTierId1, actualLoyaltyTiersTierId1, "Expected LoyaltyTiersTierId : " + loyaltyTiersTierId1 + " does not match with Actual value : " + actualLoyaltyTiersTierId1);

            actualLoyaltyTiersTierLevel1 = response.ValidateResponse("TierLevel", "LoyaltyCard,LoyaltyGroups-0,LoyaltyTiers-0");
            Assert.AreEqual(loyaltyTiersTierLevel1, actualLoyaltyTiersTierLevel1, "Expected LoyaltyTiersTierLevel : " + loyaltyTiersTierLevel1 + " does not match with Actual value : " + actualLoyaltyTiersTierLevel1);

            actualLoyaltyTiersRecordId2 = response.ValidateResponse("RecordId", "LoyaltyCard,LoyaltyGroups-0,LoyaltyTiers-1");
            Assert.AreEqual(loyaltyTiersRecordId2, actualLoyaltyTiersRecordId2, "Expected LoyaltyTiersRecordId : " + loyaltyTiersRecordId2 + " does not match with Actual value : " + actualLoyaltyTiersRecordId2);

            actualLoyaltyTiersDescription2 = response.ValidateResponse("Description", "LoyaltyCard,LoyaltyGroups-0,LoyaltyTiers-1");
            Assert.AreEqual(loyaltyTiersDescription2, actualLoyaltyTiersDescription2, "Expected LoyaltyTiersDescription : " + loyaltyTiersDescription2 + " does not match with Actual value : " + actualLoyaltyTiersDescription2);

            actualLoyaltyTiersTierId2 = response.ValidateResponse("TierId", "LoyaltyCard,LoyaltyGroups-0,LoyaltyTiers-1");
            Assert.AreEqual(loyaltyTiersTierId2, actualLoyaltyTiersTierId2, "Expected LoyaltyTiersTierId : " + loyaltyTiersTierId2 + " does not match with Actual value : " + actualLoyaltyTiersTierId2);

            actualLoyaltyTiersTierLevel2 = response.ValidateResponse("TierLevel", "LoyaltyCard,LoyaltyGroups-0,LoyaltyTiers-1");
            Assert.AreEqual(loyaltyTiersTierLevel2, actualLoyaltyTiersTierLevel2, "Expected LoyaltyTiersTierLevel : " + loyaltyTiersTierLevel2 + " does not match with Actual value : " + actualLoyaltyTiersTierLevel1);

            actualRewardPointsDescription = response.ValidateResponse("Description", "LoyaltyCard,RewardPoints-0");
            Assert.AreEqual(rewardPointsDescription, actualRewardPointsDescription, "Expected RewardPointsDescription : " + rewardPointsDescription + " does not match with Actual value : " + actualRewardPointsDescription);

            actualRewardPointsIsRedeemable = response.ValidateResponse("IsRedeemable", "LoyaltyCard,RewardPoints-0");
            Assert.AreEqual(rewardPointsIsRedeemable, actualRewardPointsIsRedeemable, "Expected RewardPointsIsRedeemable : " + rewardPointsIsRedeemable + " does not match with Actual value : " + actualRewardPointsIsRedeemable);

            actualRewardPointsRewardPointCurrency = response.ValidateResponse("RewardPointCurrency", "LoyaltyCard,RewardPoints-0");
            Assert.AreEqual(rewardPointsRewardPointCurrency, actualRewardPointsRewardPointCurrency, "Expected RewardPointsRewardPointCurrency : " + rewardPointsRewardPointCurrency + " does not match with Actual value : " + actualRewardPointsRewardPointCurrency);

            actualRewardPointsRewardPointId = response.ValidateResponse("RewardPointId", "LoyaltyCard,RewardPoints-0");
            Assert.AreEqual(rewardPointsRewardPointId, actualRewardPointsRewardPointId, "Expected RewardPointsRewardPointId : " + rewardPointsRewardPointId + " does not match with Actual value : " + actualRewardPointsRewardPointId);

            actualRewardPointsIssuedPoints = response.ValidateResponse("IssuedPoints", "LoyaltyCard,RewardPoints-0");
            Assert.AreEqual(rewardPointsIssuedPoints, actualRewardPointsIssuedPoints, "Expected RewardPointsIssuedPoints : " + rewardPointsIssuedPoints + " does not match with Actual value : " + actualRewardPointsIssuedPoints);

            actualRewardPointsUsedPoints = response.ValidateResponse("UsedPoints", "LoyaltyCard,RewardPoints-0");
            Assert.AreEqual(rewardPointsUsedPoints, actualRewardPointsUsedPoints, "Expected RewardPointsUsedPoints : " + rewardPointsUsedPoints + " does not match with Actual value : " + actualRewardPointsUsedPoints);

            actualRewardPointsExpiredPoints = response.ValidateResponse("ExpiredPoints", "LoyaltyCard,RewardPoints-0");
            Assert.AreEqual(rewardPointsExpiredPoints, actualRewardPointsExpiredPoints, "Expected RewardPointsExpiredPoints : " + rewardPointsExpiredPoints + " does not match with Actual value : " + actualRewardPointsExpiredPoints);

            actualRewardPointsActivePoints = response.ValidateResponse("ActivePoints", "LoyaltyCard,RewardPoints-0");
            Assert.AreEqual(rewardPointsActivePoints, actualRewardPointsActivePoints, "Expected RewardPointsRewardPointId : " + rewardPointsActivePoints + " does not match with Actual value : " + actualRewardPointsActivePoints);
            
            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetLoyaltyCardStatus")]
        [WorkItem(134435)]
        [Description("Verify the GetLoyaltyCardStatus method with an invalid loyalty card number")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134435", DataAccessMethod.Sequential)]
        public void VerifyTheGetloyaltycardstatusMethodWithAnInvalidLoyaltyCardNumber()
        {
            #region Local Variables

            Object response = null;
            Object actualCardNumber = null;
            Object actualCardTenderType = null;
            Object actualCustomerAccount = null;
            Object actualLoyaltyGroups = null;
            Object actualRewardPoints = null;
            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string headerName = TestContext.DataRow["loyaltyCardNumber"].ToString().Split('=')[0];
            string headerValue = TestContext.DataRow["loyaltyCardNumber"].ToString().Split('=')[1];

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();

            string cardNUmber = TestContext.DataRow["CardNumber"].ToString();
            string cardTenderType = TestContext.DataRow["CardTenderType"].ToString();
            string customerAccount = TestContext.DataRow["CustomerAccount"].ToString();
            string loyaltyGroups = TestContext.DataRow["LoyaltyGroups"].ToString();
            string rewardPoints = TestContext.DataRow["RewardPoints"].ToString();

            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName, headerValue);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response as - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualCardNumber = string.IsNullOrEmpty((string)response.ValidateResponse("CardNumber", "LoyaltyCard")) == true ? "None" : response.ValidateResponse("CardNumber", "LoyaltyCard");
            Assert.AreEqual(cardNUmber, actualCardNumber, "Expected CardNumber : " + cardNUmber + " does not match with Actual value : " + actualCardNumber);

            actualCardTenderType = string.IsNullOrEmpty((string)response.ValidateResponse("CardTenderType", "LoyaltyCard")) == true ? "None" : response.ValidateResponse("CardTenderType", "LoyaltyCard");
            Assert.AreEqual(cardTenderType, actualCardTenderType, "Expected CardTenderType : " + cardTenderType + " does not match with Actual value : " + actualCardTenderType);

            actualCustomerAccount = string.IsNullOrEmpty((string)response.ValidateResponse("CustomerAccount", "LoyaltyCard")) == true ? "None" : response.ValidateResponse("CustomerAccount", "LoyaltyCard");
            Assert.AreEqual(customerAccount, actualCustomerAccount, "Expected CustomerAccount : " + customerAccount + " does not match with Actual value : " + actualCustomerAccount);

            actualLoyaltyGroups = ((IList<string>)response.ValidateResponse("LoyaltyGroups", "LoyaltyCard")).Count == 0 ? "None" : response.ValidateResponse("LoyaltyGroups", "LoyaltyCard");
            Assert.AreEqual(loyaltyGroups, actualLoyaltyGroups, "Expected CompanyCurrency : " + loyaltyGroups + " does not match with Actual value : " + string.Join("\n", actualLoyaltyGroups));

            actualRewardPoints = ((IList<string>)response.ValidateResponse("RewardPoints", "LoyaltyCard")).Count == 0 ? "None" : response.ValidateResponse("RewardPoints", "LoyaltyCard");
            Assert.AreEqual(loyaltyGroups, actualRewardPoints, "Expected CompanyCurrency : " + loyaltyGroups + " does not match with Actual value : " + string.Join("\n", actualRewardPoints));

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetLoyaltyCardStatus")]
        [WorkItem(134436)]
        [Description("Verify the GetLoyaltyCardStatus method with an invalid resource path")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134436", DataAccessMethod.Sequential)]
        public void VerifyTheGetloyaltycardstatusMethodWithAnInvalidResourcePath()
        {
            #region Local Variables

            Object response = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string headerName = TestContext.DataRow["loyaltyCardNumber"].ToString().Split('=')[0];
            string headerValue = TestContext.DataRow["loyaltyCardNumber"].ToString().Split('=')[1];

            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[0].Trim();
            string statusDescription = TestContext.DataRow["StatusDescription"].ToString();
            
            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName, headerValue);

            response = get.Execute();


            #region Response Header verification

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response as - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "Loyalty Card status are coming while giving invalid resource path : " + get.ResponseContent);

            #endregion

            #endregion
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        
    }
}
