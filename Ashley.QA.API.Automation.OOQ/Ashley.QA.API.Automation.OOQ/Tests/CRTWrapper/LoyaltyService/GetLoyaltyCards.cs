﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashley.QA.APILibrary;
using System.Net;
using System.IO;

namespace Ashley.QA.API.Automation.OOQ.Tests.CRTWrapper.LoyaltyService
{
    /// <summary>
    /// Summary description for GetLoyaltyCards
    /// </summary>
    [TestClass]
    public class GetLoyaltyCards
    {
        public GetLoyaltyCards()
        {
            //
            // TODO: Add constructor logic here
            //
        }

       
       // [TestMethod]
        [WorkItem(134424)]
        [Description("Verify if the GetLoyaltyCards method of Loyalty Service returns the Loyalty cards for the Card holders")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134424", DataAccessMethod.Sequential)]
        public void VerifyIfTheGetLoyaltyCardsOfLoyaltyServiceReturnsTheLoyaltyCardsForTheCardHolders()
        {
            #region Local Variables

            Object response = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string customerId = TestContext.DataRow["CustomerId"].ToString();
            string customerIdEncrypt = TestContext.DataRow["CustomerIdEncrypted"].ToString();
            Dictionary<string, string> dic_cookies = new Dictionary<string, string>();
            dic_cookies.Add("cid", customerIdEncrypt);

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddCookieParameter("cid", customerIdEncrypt);
          //  get.AddQueryString("loyaltyCardNumber", "000001");
            response = get.Execute();
            var v = get.ResponseContent;
            #region Response Header verification

           // Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            //Assert.IsTrue(response.ValidateStatusDescription(errorMessage), "Error is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "State Provinces are coming for given invalid request : " + get.ResponseContent);

            #endregion

            #endregion
        }




        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        
    }
}
