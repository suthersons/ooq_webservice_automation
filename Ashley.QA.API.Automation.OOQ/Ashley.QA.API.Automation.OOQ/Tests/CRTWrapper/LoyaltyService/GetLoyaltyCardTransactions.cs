﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashley.QA.APILibrary;

namespace Ashley.QA.API.Automation.OOQ.Tests.CRTWrapper.LoyaltyService
{
    /// <summary>
    /// Summary description for GetLoyaltyCardTransactions
    /// </summary>
    [TestClass]
    public class GetLoyaltyCardTransactions
    {
        public GetLoyaltyCardTransactions()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        [TestMethod]
        [TestCategory("GetLoyaltyCardTransactions")]
        [WorkItem(135748)]
        [Description("Verify the GetLoyaltyCardTransactions method with valid input details")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "135748", DataAccessMethod.Sequential)]
        public void VerifyTheGetLoyaltyCardTransactionsWithValidInputDetails()
        {
            #region Local Variables

            Object response = null;
            Object actualLoyaltyCardTransactions = null;
            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;

            #endregion

            #region TestData

            string headerName_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[0];
            string headerValue_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[1];

            string headerName_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[0];
            string headerValue_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[1];

            string headerName_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[0];
            string headerValue_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[1];

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string loyaltyCardTransactions = TestContext.DataRow["LoyaltyCardTransactions"].ToString();
                        
            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_LoyaltyCardNo, headerValue_LoyaltyCardNo);
            get.AddQueryString(headerName_RewardPointId, headerValue_RewardPointId);
            get.AddQueryString(headerName_TopRows, headerValue_TopRows);

            response = get.Execute();
            
            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualLoyaltyCardTransactions = ((IList<string>)response.ValidateResponse("LoyaltyCardTransactions")).Count == 0 ? "None" : response.ValidateResponse("LoyaltyCardTransactions");
            Assert.AreEqual(loyaltyCardTransactions, actualLoyaltyCardTransactions, "Expected LoyaltyCardTransactions values are coming for the given request: " + string.Join("\n", ((IList<string>)response.ValidateResponse("LoyaltyCardTransactions"))));

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion

        }

        [TestMethod]
        [TestCategory("GetLoyaltyCardTransactions")]
        [WorkItem(135749)]
        [Description("Verify the GetLoyaltyCardTransactions method with an invalid loyalty card number")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "135749", DataAccessMethod.Sequential)]
        public void VerifyTheGetLoyaltyCardTransactionsWithAnInvalidLoyaltyCardNumber()
        {
            #region Local Variables

            Object response = null;
            Object actualLoyaltyCardTransactions = null;
            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;

            #endregion

            #region TestData

            string headerName_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[0];
            string headerValue_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[1];

            string headerName_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[0];
            string headerValue_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[1];

            string headerName_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[0];
            string headerValue_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[1];

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string loyaltyCardTransactions = TestContext.DataRow["LoyaltyCardTransactions"].ToString();

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_LoyaltyCardNo, headerValue_LoyaltyCardNo);
            get.AddQueryString(headerName_RewardPointId, headerValue_RewardPointId);
            get.AddQueryString(headerName_TopRows, headerValue_TopRows);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualLoyaltyCardTransactions = ((IList<string>)response.ValidateResponse("LoyaltyCardTransactions")).Count == 0 ? "None" : response.ValidateResponse("LoyaltyCardTransactions");
            Assert.AreEqual(loyaltyCardTransactions, actualLoyaltyCardTransactions, "Expected LoyaltyCardTransactions values are coming for the given request: " + string.Join("\n", ((IList<string>)response.ValidateResponse("LoyaltyCardTransactions"))));

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion

        }
        

        [TestMethod]
        [TestCategory("GetLoyaltyCardTransactions")]
        [WorkItem(135750)]
        [Description("Verify the GetLoyaltyCardTransactions with an invalid Reward point Id")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "135750", DataAccessMethod.Sequential)]
        public void VerifyTheGetLoyaltyCardTransactionsWithAnInvalidRewardPointId()
        {
            #region Local Variables

            Object response = null;
            Object actualLoyaltyCardTransactions = null;
            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;

            #endregion

            #region TestData

            string headerName_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[0];
            string headerValue_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[1];

            string headerName_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[0];
            string headerValue_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[1];

            string headerName_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[0];
            string headerValue_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[1];

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string loyaltyCardTransactions = TestContext.DataRow["LoyaltyCardTransactions"].ToString();

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_LoyaltyCardNo, headerValue_LoyaltyCardNo);
            get.AddQueryString(headerName_RewardPointId, headerValue_RewardPointId);
            get.AddQueryString(headerName_TopRows, headerValue_TopRows);

            response = get.Execute();
            
            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualLoyaltyCardTransactions = ((IList<string>)response.ValidateResponse("LoyaltyCardTransactions")).Count == 0 ? "None" : response.ValidateResponse("LoyaltyCardTransactions");
            Assert.AreEqual(loyaltyCardTransactions, actualLoyaltyCardTransactions, "Expected LoyaltyCardTransactions values are coming for the given request: " + string.Join("\n", ((IList<string>)response.ValidateResponse("LoyaltyCardTransactions"))));

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion

        }


        [TestMethod]
        [TestCategory("GetLoyaltyCardTransactions")]
        [WorkItem(135751)]
        [Description("Verify the GetLoyaltyCardTransactions method with an invalid TopRow value")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "135751", DataAccessMethod.Sequential)]
        public void VerifyTheGetLoyaltyCardTransactionsWithAnInvalidTopRowValue()
        {
            #region Local Variables

            Object response = null;

            #endregion

            #region TestData

            string headerName_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[0];
            string headerValue_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[1];

            string headerName_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[0];
            string headerValue_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[1];

            string headerName_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[0];
            string headerValue_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[1];

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[0].Trim();
            string statusDescription = TestContext.DataRow["StatusDescription"].ToString();
        

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_LoyaltyCardNo, headerValue_LoyaltyCardNo);
            get.AddQueryString(headerName_RewardPointId, headerValue_RewardPointId);
            get.AddQueryString(headerName_TopRows, headerValue_TopRows);

            response = get.Execute();
            
            #region Response Header verification

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response as - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "State Provinces are coming for given invalid request : " + get.ResponseContent);

            #endregion

            #endregion

        }

        [TestMethod]
        [TestCategory("GetLoyaltyCardTransactions")]
        [WorkItem(135752)]
        [Description("Verify the GetLoyaltyCardTransactions method with an invalid resource path")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "135752", DataAccessMethod.Sequential)]
        public void VerifyTheGetLoyaltyCardTransactionsWithAnInvalidResourcePath()
        {
            #region Local Variables

            Object response = null;

            #endregion

            #region TestData

            string headerName_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[0];
            string headerValue_LoyaltyCardNo = TestContext.DataRow["LoyaltyCardNumber"].ToString().Split('=')[1];

            string headerName_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[0];
            string headerValue_RewardPointId = TestContext.DataRow["RewardPointId"].ToString().Split('=')[1];

            string headerName_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[0];
            string headerValue_TopRows = TestContext.DataRow["TopRows"].ToString().Split('=')[1];

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[0].Trim();
            string statusDescription = TestContext.DataRow["StatusDescription"].ToString();


            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName_LoyaltyCardNo, headerValue_LoyaltyCardNo);
            get.AddQueryString(headerName_RewardPointId, headerValue_RewardPointId);
            get.AddQueryString(headerName_TopRows, headerValue_TopRows);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response as - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "State Provinces are coming for given invalid request : " + get.ResponseContent);

            #endregion

            #endregion

        }



        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

       
    }
}
