﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashley.QA.APILibrary;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using Ashley.QA.API.Automation.OOQ.Properties;

namespace Ashley.QA.API.Automation.OOQ.Tests.CRTWrapper.ChannelService
{
    /// <summary>
    /// Summary description for GetStateProvinceInfo
    /// </summary>
    [TestClass]
    public class GetStateProvinceInfo
    {
        public GetStateProvinceInfo()
        {

        }


        [TestMethod]
        [TestCategory("GetStateProvinceInfo")]
        [WorkItem(134264)]
        [Description("Verify the GetStateProvinceInfo method with valid Country Code")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134264", DataAccessMethod.Sequential)]
        public void VerifyTheGetstateprovinceinfoMethodWithValidCountryCode()
        {
            
            #region Local Variables

            Object response = null;
            DataTable actualResponseContent = null; 
            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;
            Object actualTopLevelContent = null;

            List<Tuple<bool, Object[], Object[]>> list_output = new List<Tuple<bool, Object[], Object[]>>();

            #endregion

            #region TestData

            string headerName = TestContext.DataRow["countryCode"].ToString().Split('=')[0];
            string headerValue = TestContext.DataRow["countryCode"].ToString().Split('=')[1];
            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() +"/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();

            DataTable expectedResponseContentFromDB = ConnectSQL.GetValueFromSQLTable(OOQSettings.Default.ConnectionString, OOQSettings.Default.QueryString);

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName, headerValue);

            response = get.Execute();
                        
            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualTopLevelContent = get.ResponseContent.ValidateResponse("StateProvinces");

            actualResponseContent = actualTopLevelContent.GetAllJsonRespToDataTable("CountryRegionId,StateId,StateName");
            
            for (int i = 0; i < expectedResponseContentFromDB.Rows.Count; i++)
            {
                if (DataRowComparer<DataRow>.Default.Equals(expectedResponseContentFromDB.Rows[i], actualResponseContent.Rows[i]) == false)
                {
                    list_output.Add(Tuple.Create<bool, Object[], Object[]>(DataRowComparer<DataRow>.Default.Equals(expectedResponseContentFromDB.Rows[i], actualResponseContent.Rows[i]), expectedResponseContentFromDB.Rows[i].ItemArray, actualResponseContent.Rows[i].ItemArray));
                }
            }

            if (list_output.Count > 0)
            {
                List<string> list_mismatchRows = new List<string>();
                foreach (Tuple<bool, Object[], Object[]> tupleSingle in list_output)
                {
                    list_mismatchRows.Add("Expected row Value : " + string.Join("|", tupleSingle.Item2) + " Actual Row value : " + string.Join("|", tupleSingle.Item3));
                }
                Assert.Fail("Following rows are mismatched for the Column 'CountryRegionId','StateId','StateName' :\n " + string.Join("\n", list_mismatchRows));
            }

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetStateProvinceInfo")]
        [WorkItem(134266)]
        [Description("Verify the GetStateProvinceInfo method with an invalid Country Code")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134266", DataAccessMethod.Sequential)]
        public void VerifyTheGetstateprovinceinfoMethodWithAnInvalidCountryCode()
        {
            #region Local Variables

            Object response = null;
            Object actualStateProvinces = null;
            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;

            #endregion

            #region TestData

            string headerName = TestContext.DataRow["countryCode"].ToString().Split('=')[0];
            string headerValue = TestContext.DataRow["countryCode"].ToString().Split('=')[1];
            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);

            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();

            string stateProvinces = TestContext.DataRow["StateProvinces"].ToString();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName, headerValue);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response as - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualStateProvinces = ((IList<string>)response.ValidateResponse("StateProvinces")).Count == 0 ? "None" : response.ValidateResponse("StateProvinces");
           // (actualStateProvinces).Add(s.ToString());
            Assert.AreEqual(stateProvinces, actualStateProvinces.ToString(), "State Provinces are coming for given request : " + string.Join("\n", actualStateProvinces));

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));
            
            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetStateProvinceInfo")]
        [WorkItem(134267)]
        [Description("Verify the GetStateProvinceInfo method with an invalid resource path")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134267", DataAccessMethod.Sequential)]
        public void VerifyTheGetstateprovinceinfoMethodWithAnInvalidResourcePath()
        {
            #region Local Variables

            Object response = null;
            
            #endregion

            #region TestData

            string headerName = TestContext.DataRow["countryCode"].ToString().Split('=')[0];
            string headerValue = TestContext.DataRow["countryCode"].ToString().Split('=')[1];
            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[0].Trim();
            string statusDescription = TestContext.DataRow["StatusDescription"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            get.AddQueryString(headerName, headerValue);

            response = get.Execute();
            
            #region Response Header verification

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response as - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response as - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "State Provinces are coming for given invalid request : " + get.ResponseContent);
            
            #endregion

            #endregion
        }



        [TestMethod]
        [TestCategory("GetStateProvinceInfo")]
        [WorkItem(134446)]
        [Description("Verify the GetStateProvinceInfo method Incorrect or Without country code parameter")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134446", DataAccessMethod.Sequential)]
        public void VerifyTheGetstateprovinceinfoMethodIncorrectOrWithoutCountryCodeParameter()
        {
            #region Local Variables

            Object response = null;

            #endregion

            #region TestData

            string headerName = TestContext.DataRow["countryCode"].ToString().Split('=')[0];
            string headerValue = TestContext.DataRow["countryCode"].ToString().Split('=')[1];
            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string statusCode = TestContext.DataRow["StatusCode"].ToString();
            string statusDescription = TestContext.DataRow["StatusDescription"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);
            if (!string.IsNullOrEmpty(headerName))
            {
                get.AddQueryString(headerName, headerValue);
            }
            
            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response as - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the response as - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "State Provinces are coming for given invalid request : " + get.ResponseContent);

            #endregion

            #endregion
        }


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

    }
}
