﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ashley.QA.APILibrary;

namespace Ashley.QA.API.Automation.OOQ.Tests.CRTWrapper.ChannelService
{
    /// <summary>
    /// Summary description for GetChannelConfiguration
    /// </summary>
    [TestClass]
    public class GetChannelConfiguration
    {
        public GetChannelConfiguration()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        [TestMethod]
        [TestCategory("GetChannelConfiguration")]
        [WorkItem(134418)]
        [Description("Verify if the GetChannelConfiguration method returns the Channel Configuration details")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134418", DataAccessMethod.Sequential)]
        public void VerifyIfTheGetchannelconfigurationMethodReturnsTheChannelConfigurationDetails()
        {
            #region Local Variables

            Object response = null;
            Object actualCanRetry = null;
            Object actualRedirectUrl = null;
            Object actualErrors = null;
            Object actualRecordId = null;
            Object actualInventLocation = null;
            Object actualCurrency = null;
            Object actualCompCurrency = null;
            Object actualPriceIncludesSalesTax = null;
            Object actualCountryRegionId = null;
            Object actualDefaultLanguageId = null;
            Object actualPickupDeliveryModeCode = null;
            Object actualBingMapsApiKey = null;
            Object actualTimeZoneInfoId = null;
            Object actualEmailDeliveryModeCode = null;
            Object actualGiftCardItemId = null;
            Object actualCurrencyStringTemplate = null;          

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string recordId = TestContext.DataRow["RecordId"].ToString();
            string inventLocation = TestContext.DataRow["InventLocation"].ToString();
            string currency = TestContext.DataRow["Currency"].ToString();
            string compCurrency = TestContext.DataRow["CompanyCurrency"].ToString();
            string priceIncludesSalesTax = TestContext.DataRow["PriceIncludesSalesTax"].ToString();
            string countryRegionId = TestContext.DataRow["CountryRegionId"].ToString();
            string defaultLanguageId  = TestContext.DataRow["DefaultLanguageId"].ToString();
            string pickupDeliveryModeCode = TestContext.DataRow["PickupDeliveryModeCode"].ToString();
            string bingMapsApiKey = TestContext.DataRow["BingMapsApiKey"].ToString();
            string timeZoneInfoId = TestContext.DataRow["TimeZoneInfoId"].ToString();
            string emailDeliveryModeCode = TestContext.DataRow["EmailDeliveryModeCode"].ToString();
            string giftCardItemId = TestContext.DataRow["GiftCardItemId"].ToString();
            string currencyStringTemplate = TestContext.DataRow["CurrencyStringTemplate"].ToString();

            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            actualRecordId = response.ValidateResponse("RecordId","ChannelConfiguration");
            Assert.AreEqual(recordId, actualRecordId, "Expected RecordID : " + recordId + " does not match with Actual value : " + actualRecordId);

            actualInventLocation = response.ValidateResponse("InventLocation", "ChannelConfiguration");
            Assert.AreEqual(inventLocation, actualInventLocation, "Expected InventLocation : " + inventLocation + " does not match with Actual value : " + actualInventLocation);

            actualCurrency = response.ValidateResponse("Currency", "ChannelConfiguration");
            Assert.AreEqual(currency, actualCurrency, "Expected Currency : " + inventLocation + " does not match with Actual value : " + actualCurrency);

            actualCompCurrency = response.ValidateResponse("CompanyCurrency", "ChannelConfiguration");
            Assert.AreEqual(compCurrency, actualCompCurrency, "Expected CompanyCurrency : " + compCurrency + " does not match with Actual value : " + actualCompCurrency);

            actualPriceIncludesSalesTax = response.ValidateResponse("PriceIncludesSalesTax", "ChannelConfiguration");
            Assert.AreEqual(priceIncludesSalesTax, actualPriceIncludesSalesTax, "Expected PriceIncludesSalesTax : " + priceIncludesSalesTax + " does not match with Actual value : " + actualPriceIncludesSalesTax);

            actualCountryRegionId = response.ValidateResponse("CountryRegionId", "ChannelConfiguration");
            Assert.AreEqual(countryRegionId, actualCountryRegionId, "Expected CountryRegionId : " + countryRegionId + " does not match with Actual value : " + actualCountryRegionId);

            actualDefaultLanguageId = response.ValidateResponse("DefaultLanguageId", "ChannelConfiguration");
            Assert.AreEqual(defaultLanguageId, actualDefaultLanguageId, "Expected DefaultLanguageId : " + defaultLanguageId + " does not match with Actual value : " + actualDefaultLanguageId);

            actualPickupDeliveryModeCode = string.IsNullOrEmpty((string)response.ValidateResponse("PickupDeliveryModeCode", "ChannelConfiguration")) == true ? "None" : response.ValidateResponse("PickupDeliveryModeCode", "ChannelConfiguration");
            Assert.AreEqual(pickupDeliveryModeCode, actualPickupDeliveryModeCode, "Expected PickupDeliveryModeCode : " + pickupDeliveryModeCode + " does not match with Actual value : " + actualPickupDeliveryModeCode);

            actualBingMapsApiKey = string.IsNullOrEmpty((string)response.ValidateResponse("BingMapsApiKey", "ChannelConfiguration")) == true ? "None" : response.ValidateResponse("BingMapsApiKey", "ChannelConfiguration");
            Assert.AreEqual(bingMapsApiKey, actualBingMapsApiKey, "Expected BingMapsApiKey : " + bingMapsApiKey + " does not match with Actual value : " + actualBingMapsApiKey);

            actualTimeZoneInfoId = response.ValidateResponse("TimeZoneInfoId", "ChannelConfiguration");
            Assert.AreEqual(timeZoneInfoId, actualTimeZoneInfoId, "Expected TimeZoneInfoId : " + timeZoneInfoId + " does not match with Actual value : " + actualTimeZoneInfoId);

            actualEmailDeliveryModeCode = string.IsNullOrEmpty((string)response.ValidateResponse("EmailDeliveryModeCode", "ChannelConfiguration")) == true ? "None" : response.ValidateResponse("EmailDeliveryModeCode", "ChannelConfiguration");
            Assert.AreEqual(emailDeliveryModeCode, actualEmailDeliveryModeCode, "Expected EmailDeliveryModeCode : " + emailDeliveryModeCode + " does not match with Actual value : " + actualEmailDeliveryModeCode);

            actualGiftCardItemId = string.IsNullOrEmpty((string)response.ValidateResponse("GiftCardItemId", "ChannelConfiguration")) == true ? "None" : response.ValidateResponse("GiftCardItemId", "ChannelConfiguration");
            Assert.AreEqual(giftCardItemId, actualGiftCardItemId, "Expected GiftCardItemId : " + giftCardItemId + " does not match with Actual value : " + actualGiftCardItemId);

            actualCurrencyStringTemplate = response.ValidateResponse("CurrencyStringTemplate", "ChannelConfiguration");
            Assert.AreEqual(currencyStringTemplate, actualCurrencyStringTemplate, "Expected CurrencyStringTemplate : " + currencyStringTemplate + " does not match with Actual value : " + actualCurrencyStringTemplate);

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given invalid request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion
        }


        [TestMethod]
        [TestCategory("GetChannelConfiguration")]
        [WorkItem(134419)]
        [Description("Verify the GetChannelConfiguration method with an invalid resource path")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134419", DataAccessMethod.Sequential)]
        public void VerifyTheGetchannelconfigurationMethodWithAnInvalidResourcePath()
        {
            #region Local Variables

            Object response = null;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[0].Trim();
            string errorMessage = TestContext.DataRow["ErrorMessage"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(errorMessage), "Error is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "State Provinces are coming for given invalid request : " + get.ResponseContent);

            #endregion

            #endregion
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
                
    }
}

