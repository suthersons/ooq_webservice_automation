﻿using Ashley.QA.APILibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ashley.QA.API.Automation.OOQ.Tests.CRTWrapper.ChannelService
{
    [TestClass]
    public class GetChannelTenderTypes
    {

        public GetChannelTenderTypes()
        {
        }


        [TestMethod]
        [TestCategory("GetChannelTenderTypes")]
        [WorkItem(134420)]
        [Description("Verify if the GetChannelTenderTypes method  returns the Channel Tender type details")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134420", DataAccessMethod.Sequential)]
        public void VerifyIfTheGetchanneltendertypesMethodReturnsTheChannelTenderTypeDetails()
        {
            #region Local Variables

            Object response = null;
            Object actualHasCreditCardPayment = string.Empty;
            Object actualHasGiftCardPayment = string.Empty;
            Object actualHasLoyaltyCardPayment = string.Empty;
            Object actualCanRetry = string.Empty;
            Object actualRedirectUrl = string.Empty;
            Object actualErrors = string.Empty;

            #endregion

            #region TestData

           // string headerName = TestContext.DataRow["countryCode"].ToString().Split('=')[0];
           // string headerValue = TestContext.DataRow["countryCode"].ToString().Split('=')[1];
            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string hasCreditCardPayment = TestContext.DataRow["HasCreditCardPayment"].ToString();
            string hasGiftCardPayment = TestContext.DataRow["HasGiftCardPayment"].ToString();
            string hasLoyaltyCardPayment = TestContext.DataRow["HasLoyaltyCardPayment"].ToString();
            string canRetry = TestContext.DataRow["CanRetry"].ToString();
            string redirectURL = TestContext.DataRow["RedirectUrl"].ToString();
            string errors = TestContext.DataRow["Errors"].ToString();
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string statusDescription = TestContext.DataRow["StatusCode"].ToString().Split('-')[1].Trim();
            string contentType = TestContext.DataRow["ContentType"].ToString() + "; charset=utf-8";
            string server = TestContext.DataRow["ServerHeader"].ToString() + "/8.0";

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(get.ResponseContent.IsJSON(), "Given response format is not a valid content-Type " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateContentType(contentType), "Content Type is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Content-Type"]);

            Assert.IsTrue(response.ValidateServer(server), "Server is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["Server"]);

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(statusDescription), "Status Description is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion
            
            #region Actual Response Validation

            actualHasCreditCardPayment = response.ValidateResponse("HasCreditCardPayment");
            Assert.AreEqual(hasCreditCardPayment, actualHasCreditCardPayment, "Expected payment value : " + hasCreditCardPayment + " does not match with Actual value : " + actualHasCreditCardPayment);

            actualHasGiftCardPayment = response.ValidateResponse("HasGiftCardPayment");
            Assert.AreEqual(hasGiftCardPayment, actualHasGiftCardPayment, "Expected payment value : " + hasGiftCardPayment + " does not match with Actual value : " + actualHasGiftCardPayment);

            actualHasLoyaltyCardPayment = response.ValidateResponse("HasLoyaltyCardPayment");
            Assert.AreEqual(hasLoyaltyCardPayment, actualHasLoyaltyCardPayment, "Expected payment value : " + hasLoyaltyCardPayment + " does not match with Actual value : " + actualHasLoyaltyCardPayment);

            actualCanRetry = response.ValidateResponse("CanRetry");
            Assert.AreEqual(canRetry, actualCanRetry, "Expected retry value : " + canRetry + " does not match with Actual value : " + actualCanRetry);

            actualRedirectUrl = string.IsNullOrEmpty((string)response.ValidateResponse("RedirectUrl")) == true ? "None" : response.ValidateResponse("RedirectUrl");
            Assert.AreEqual(redirectURL, actualRedirectUrl, "Url redirection is happening for the given request : " + actualRedirectUrl);

            actualErrors = ((IList<string>)response.ValidateResponse("Errors")).Count == 0 ? "None" : response.ValidateResponse("Errors");
            Assert.AreEqual(errors, actualErrors, "Erros are coming for given request : " + string.Join("\n", actualErrors));

            #endregion

            #endregion
        }



        [TestMethod]
        [TestCategory("GetChannelTenderTypes")]
        [WorkItem(134421)]
        [Description("Verify the GetChannelTenderTypes method with an invalid resource path")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://10.9.8.197:8080/tfs/Ashley;Ashley.QA", "134421", DataAccessMethod.Sequential)]
        public void VerifyTheGetchanneltendertypesMethodWithAnInvalidResourcePath()
        {
            #region Local Variables

            Object response = null;
            Object actualHasCreditCardPayment = string.Empty;
            Object actualHasGiftCardPayment = string.Empty;
            Object actualHasLoyaltyCardPayment = string.Empty;
            Object actualCanRetry = string.Empty;
            Object actualRedirectUrl = string.Empty;
            Object actualErrors = string.Empty;

            #endregion

            #region TestData

            string endPoint = TestContext.DataRow["EndPoint"].ToString();
            string resourcePath = TestContext.DataRow["ResourcePath"].ToString();
            Uri uri = new Uri(endPoint + resourcePath);
            string statusCode = TestContext.DataRow["StatusCode"].ToString().Split('-')[0].Trim();
            string errorMessage = TestContext.DataRow["ErrorMessage"].ToString();

            #endregion

            #region TestSteps

            AshleyAPI.GET get = new AshleyAPI.GET(uri);

            response = get.Execute();

            #region Response Header verification

            Assert.IsTrue(response.ValidateStatusCode(statusCode), "Status Code is not displayed properly in the actual response - " + get.ResponseStatusCodeDescription);

            Assert.IsTrue(response.ValidateStatusDescription(errorMessage), "Error is not displayed properly in the actual response - " + AshleyAPI.ResponseHeaders["StatusDescription"]);

            #endregion

            #region Actual Response Validation

            Assert.IsTrue(response == null, "State Provinces are coming for given invalid request : " + get.ResponseContent);

            #endregion

            #endregion
        }



        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
    }
    
}
